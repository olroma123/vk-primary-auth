"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var cookie_1 = __importDefault(require("cookie"));
var query_string_1 = __importDefault(require("query-string"));
var VkSecurityHelper = /** @class */ (function () {
    function VkSecurityHelper() {
    }
    VkSecurityHelper.parseAllCookies = function (cookies) {
        return cookies.map(function (cookieFromVk) {
            var parsedCookie = cookie_1.default.parse(cookieFromVk);
            parsedCookie.key = Object.keys(parsedCookie)[0];
            parsedCookie.value = parsedCookie[parsedCookie.key];
            return {
                key: parsedCookie.key,
                value: parsedCookie.value,
                path: parsedCookie.path,
                domain: parsedCookie.domain,
                expires: parsedCookie.expires
            };
        });
    };
    VkSecurityHelper.joinCookies = function (cookies) {
        return cookies.map(function (current) {
            return cookie_1.default.serialize(current.key, current.value);
        }).join('; ');
    };
    return VkSecurityHelper;
}());
var VkSecurity = /** @class */ (function () {
    function VkSecurity() {
        this.__cookieContol = {};
        axios_1.default.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        axios_1.default.defaults.headers.post['User-Agent'] = 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36';
        axios_1.default.defaults.headers.post['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
        // Add custom status validation for 302 http status code
        // It is need because the default "validateStatus" throw an error on 302 status
        axios_1.default.defaults.maxRedirects = 0;
        axios_1.default.defaults.validateStatus = function (status) {
            return status >= 200 && status <= 302;
        };
    }
    VkSecurity.prototype.setCurrentCookie = function (cookie) {
        this.__cookieContol.current = cookie;
    };
    VkSecurity.prototype.setTwoFactorCookie = function (cookie) {
        this.__cookieContol.twoFactor = cookie;
    };
    VkSecurity.prototype.getCurrentCookie = function () {
        return this.__cookieContol.current;
    };
    VkSecurity.prototype.getTwoFactorCookie = function () {
        return this.__cookieContol.twoFactor;
    };
    VkSecurity.prototype.getCurrentCookieString = function () {
        if (!this.__cookieContol.current)
            return null;
        return VkSecurityHelper.joinCookies(this.__cookieContol.current);
    };
    VkSecurity.prototype.getTwoFactorCookieString = function () {
        if (!this.__cookieContol.twoFactor)
            return null;
        return VkSecurityHelper.joinCookies(this.__cookieContol.twoFactor);
    };
    VkSecurity.prototype.getSecurityParamsFromAuthPage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, cookies;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default({
                            url: 'https://vk.com',
                            method: 'POST',
                        })];
                    case 1:
                        response = _a.sent();
                        cookies = VkSecurityHelper.parseAllCookies(response.headers['set-cookie']);
                        this.setCurrentCookie(cookies);
                        this.setTwoFactorCookie(cookies);
                        return [2 /*return*/, {
                                ip_h: response.data.match(/name=\"ip_h\" value=\"(.*?)\"/s)[1],
                                lg_h: response.data.match(/name=\"lg_h\" value=\"(.*?)\"/s)[1]
                            }];
                }
            });
        });
    };
    VkSecurity.prototype.validateRemixsid = function (remixsidCookie) {
        return __awaiter(this, void 0, void 0, function () {
            var response, userObject;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default({
                            url: 'https://vk.com/feed2.php',
                            headers: {
                                cookie: remixsidCookie.key + "=" + remixsidCookie.value
                            }
                        })];
                    case 1:
                        response = _a.sent();
                        if (!response.data.user) {
                            try {
                                userObject = JSON.parse(response.data);
                            }
                            catch (e) {
                                return [2 /*return*/, {
                                        status: false,
                                        data: {
                                            error: 'onJsonBadSyntax'
                                        }
                                    }];
                            }
                        }
                        else
                            userObject = response.data;
                        if (!userObject.user || userObject.user.id === -1) {
                            return [2 /*return*/, {
                                    status: false,
                                    data: {
                                        error: 'onRemixsidWrong'
                                    }
                                }];
                        }
                        return [2 /*return*/, {
                                status: true,
                                data: {
                                    payload: {
                                        userId: userObject.user.id,
                                        remixsid: remixsidCookie.value
                                    }
                                }
                            }];
                }
            });
        });
    };
    VkSecurity.prototype.sendAuthRequest = function (securityParams, credits) {
        return __awaiter(this, void 0, void 0, function () {
            var loginResponse, loginVkCookies, responseRedirectAuth, redirectAuthCookies, statusTextInResponse, successCookieFind, statusData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, axios_1.default.post("https://login.vk.com?act=login", query_string_1.default.stringify({
                            act: 'login',
                            role: 'al_frame',
                            _origin: 'https://vk.com',
                            utf8: '1',
                            email: credits.login,
                            pass: credits.password,
                            lg_h: securityParams.lg_h
                        }), {
                            headers: {
                                cookie: this.getCurrentCookieString()
                            }
                        })];
                    case 1:
                        loginResponse = _a.sent();
                        // FOR DEBUG ONLY
                        // TODO: REMOVE THIS CONDITION
                        if (loginResponse.status !== 302) {
                            console.error('RESPONSE CODE IS NOT 302', loginResponse);
                            return [2 /*return*/, {
                                    status: false,
                                    data: {
                                        error: 'onWrongResponseCode'
                                    }
                                }];
                        }
                        if (!loginResponse.headers['set-cookie']) {
                            return [2 /*return*/, {
                                    status: false,
                                    data: {
                                        error: 'onBadPassword'
                                    }
                                }];
                        }
                        loginVkCookies = VkSecurityHelper.parseAllCookies(loginResponse.headers['set-cookie']);
                        return [4 /*yield*/, axios_1.default.post(loginResponse.headers.location, {
                                headers: {
                                    cookie: VkSecurityHelper.joinCookies(loginResponse.headers['set-cookie'])
                                }
                            })];
                    case 2:
                        responseRedirectAuth = _a.sent();
                        redirectAuthCookies = VkSecurityHelper.parseAllCookies(responseRedirectAuth.headers['set-cookie']);
                        statusTextInResponse = responseRedirectAuth.data;
                        console.log(responseRedirectAuth);
                        // Check for status of auth
                        // Captha is required
                        if (statusTextInResponse.includes('onLoginCaptcha(')) {
                            return [2 /*return*/, {
                                    status: false,
                                    data: {
                                        error: 'onLoginCaptcha'
                                    }
                                }];
                        }
                        // Wrong login or password
                        if (statusTextInResponse.includes('onLoginFailed(4')) {
                            return [2 /*return*/, {
                                    status: false,
                                    data: {
                                        error: 'onBadPassword'
                                    }
                                }];
                        }
                        // Two factor auth
                        if (statusTextInResponse.includes('act=authcheck')) {
                            return [2 /*return*/, {
                                    status: false,
                                    data: {
                                        error: 'onTwoFactorAuth'
                                    }
                                }];
                        }
                        // Account blocked
                        if (statusTextInResponse.includes('act=blocked')) {
                            return [2 /*return*/, {
                                    status: false,
                                    data: {
                                        error: 'onAccountBlocked'
                                    }
                                }];
                        }
                        successCookieFind = redirectAuthCookies.find(function (iCookie) { return iCookie.key === 'remixsid'; });
                        if (!successCookieFind) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.validateRemixsid(successCookieFind)];
                    case 3:
                        statusData = _a.sent();
                        if (statusData.status && statusData.data.payload)
                            statusData.data.payload.loginVkCookies = loginVkCookies;
                        return [2 /*return*/, statusData];
                    case 4: return [2 /*return*/, {
                            status: false,
                            data: {
                                error: 'unknow'
                            }
                        }];
                }
            });
        });
    };
    return VkSecurity;
}());
exports.default = VkSecurity;
