import axios from 'axios';
import { IVK, Types } from './types';
import cookie from 'cookie';
import queryString from 'query-string';

class VkSecurityHelper {
  public static parseAllCookies(cookies: Array<string>): Array<Types.Cookie> {
    return cookies.map((cookieFromVk: string) => {
      let parsedCookie = cookie.parse(cookieFromVk);

      parsedCookie.key = Object.keys(parsedCookie)[0];
      parsedCookie.value = parsedCookie[parsedCookie.key];

      return {
        key: parsedCookie.key,
        value: parsedCookie.value,
        path: parsedCookie.path,
        domain: parsedCookie.domain,
        expires: parsedCookie.expires
      };
    })
  }

  public static joinCookies(cookies: Array<Types.Cookie>): string {
    return cookies.map((current) => {
      return cookie.serialize(current.key, current.value);
    }).join('; ');
  }
}

export default class VkSecurity {
  private __cookieContol: IVK.Cookie = {}

  public setCurrentCookie(cookie: Array<Types.Cookie>): void {
    this.__cookieContol.current = cookie;
  }

  public setTwoFactorCookie(cookie: Array<Types.Cookie>): void {
    this.__cookieContol.twoFactor = cookie;
  }

  public getCurrentCookie(): Array<Types.Cookie> | undefined {
    return this.__cookieContol.current;
  }

  public getTwoFactorCookie(): Array<Types.Cookie> | undefined {
    return this.__cookieContol.twoFactor;
  }

  public getCurrentCookieString(): string | null {
    if (!this.__cookieContol.current) 
      return null;

    return VkSecurityHelper.joinCookies(this.__cookieContol.current);
  }

  public getTwoFactorCookieString(): string | null {
    if (!this.__cookieContol.twoFactor) 
      return null;

    return VkSecurityHelper.joinCookies(this.__cookieContol.twoFactor);
  }

  constructor() {
    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    axios.defaults.headers.post['User-Agent'] = 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36';
    axios.defaults.headers.post['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
    
    // Add custom status validation for 302 http status code
    // It is need because the default "validateStatus" throw an error on 302 status
    axios.defaults.maxRedirects = 0;
    axios.defaults.validateStatus = (status) => {
      return status >= 200 && status <= 302;
    };
  }

  public async getSecurityParamsFromAuthPage(): Promise<IVK.SecurityXSS> {
    const response = await axios({
      url: 'https://vk.com',
      method: 'POST', 
    });

    // Save cookies for the next step
    let cookies = VkSecurityHelper.parseAllCookies(response.headers['set-cookie']);

    this.setCurrentCookie(cookies);
    this.setTwoFactorCookie(cookies);

    return {
      ip_h: response.data.match(/name=\"ip_h\" value=\"(.*?)\"/s)[1],
      lg_h: response.data.match(/name=\"lg_h\" value=\"(.*?)\"/s)[1]
    }
  }

  public async validateRemixsid(remixsidCookie: Types.Cookie): Promise<Types.SecurityStatus> {
    const response = await axios({
      url: 'https://vk.com/feed2.php',
      headers: {
        cookie: `${remixsidCookie.key}=${remixsidCookie.value}`
      }
    });

    let userObject;
    if (!response.data.user) {
      try {
        userObject = JSON.parse(response.data);
      } catch (e) {
        return {
          status: false,
          data: {
            error: 'onJsonBadSyntax'
          }
        }
      }
    } else userObject = response.data;

    if (!userObject.user || userObject.user.id === -1) {
      return {
        status: false,
        data: {
          error: 'onRemixsidWrong'
        }
      }
    }

    return {
      status: true,
      data: {
        payload: {
          userId: userObject.user.id,
          remixsid: remixsidCookie.value
        }
      }
    }
  }

  public async sendAuthRequest(securityParams: IVK.SecurityXSS, credits: IVK.AuthData): Promise<Types.SecurityStatus> {
    const loginResponse = await axios.post(`https://login.vk.com?act=login`, queryString.stringify({
      act: 'login',
      role: 'al_frame',
      _origin: 'https://vk.com',
      utf8: '1',
      email: credits.login,
      pass: credits.password,
      lg_h: securityParams.lg_h
    }), {
      headers: {
        cookie: this.getCurrentCookieString()
      }
    });

    // FOR DEBUG ONLY
    // TODO: REMOVE THIS CONDITION
    if (loginResponse.status !== 302) {
      console.error('RESPONSE CODE IS NOT 302', loginResponse);
      return {
        status: false,
        data: {
          error: 'onWrongResponseCode'
        }
      }
    }

    if (!loginResponse.headers['set-cookie']) {
      return {
        status: false,
        data: {
          error: 'onBadPassword'
        }
      }
    }

    // Save cookies for .login domain
    const loginVkCookies = VkSecurityHelper.parseAllCookies(loginResponse.headers['set-cookie']);

    // Do redirect for getting auth status
    const responseRedirectAuth = await axios.post(loginResponse.headers.location, {
      headers: {
        cookie: VkSecurityHelper.joinCookies(loginResponse.headers['set-cookie'])
      }
    });

    const redirectAuthCookies = VkSecurityHelper.parseAllCookies(responseRedirectAuth.headers['set-cookie']);
    const statusTextInResponse = responseRedirectAuth.data;

    console.log(responseRedirectAuth);

    // Check for status of auth
    // Captha is required
    if (statusTextInResponse.includes('onLoginCaptcha(')) {
      return {
        status: false,
        data: {
          error: 'onLoginCaptcha'
        }
      }
    }
    
    // Wrong login or password
    if (statusTextInResponse.includes('onLoginFailed(4')) {
      return {
        status: false,
        data: {
          error: 'onBadPassword'
        }
      }
    }

    // Two factor auth
    if (statusTextInResponse.includes('act=authcheck')) {
      return {
        status: false,
        data: {
          error: 'onTwoFactorAuth'
        }
      }
    }

    // Account blocked
    if (statusTextInResponse.includes('act=blocked')) {
      return {
        status: false,
        data: {
          error: 'onAccountBlocked'
        }
      }
    }

    // Success authorize
    const successCookieFind = redirectAuthCookies.find((iCookie) => iCookie.key === 'remixsid');
    if (successCookieFind) {
      let statusData = await this.validateRemixsid(successCookieFind);

      if (statusData.status && statusData.data.payload)
        statusData.data.payload.loginVkCookies = loginVkCookies;

      return statusData;
    }

    return {
      status: false,
      data: {
        error: 'unknow'
      }
    }
  }
}