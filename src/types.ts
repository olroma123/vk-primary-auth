export declare namespace Types {
  export type Cookie = {
    key: string;
    value: string;
    path: string;
    domain: string;
    expires: string;
  }

  export type SecurityAuthData = {
    remixsid?: string,
    userId?: string,
    loginVkCookies?: Array<Cookie>
  };

  export type SecurityStatus = {
    status: boolean,
    data: {
      error?: string,
      payload?: SecurityAuthData
    }
  };
}

export declare namespace IVK {
  export type SecurityXSS = {
    ip_h: string;
    lg_h: string;
  }

  export interface Cookie {
    current?: Array<Types.Cookie>,
    twoFactor?: Array<Types.Cookie>
  }

  export type AuthData = {
    login: string;
    password: string;
  }

  export type User = {
    cookie: string,
    userid: string
  }
}