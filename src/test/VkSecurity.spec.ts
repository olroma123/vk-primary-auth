/// <reference path="../types.ts" />

import VkSecurity from '../VkSecurity';
import { expect } from 'chai';
import 'mocha';
import { IVK } from '../types';

describe('Security Methods', () => { 
  const security = new VkSecurity();
  let secParams: IVK.SecurityXSS;

  it('should return the not empty ip_h and lg_h values from vk.com', async () => {
    secParams = await security.getSecurityParamsFromAuthPage();

    expect(secParams).to.be.an('object').that.does.have.property('lg_h');
    expect(secParams).to.be.an('object').that.does.have.property('ip_h');

    expect(secParams.lg_h).to.not.be.empty;
    expect(secParams.ip_h).to.not.be.empty;
  });

  it('cookies should be set', () => {
    const currentCookie = security.getCurrentCookie();
    const twoFactorCookie = security.getTwoFactorCookie();

    expect(currentCookie).to.be.an('array').that.does.not.empty;
    expect(twoFactorCookie).to.be.an('array').that.does.not.empty;

    expect(currentCookie).to.deep.equal(twoFactorCookie);
  });

  it('should send the auth request', async () => {
    let status = await security.sendAuthRequest(secParams, {
      login: 'YOUR_LOGIN',
      password: 'YOUR_PASSWORD'
    });

    console.log(status);
  });
});