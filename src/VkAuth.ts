import { IVK } from './types';

class VkAuth {
  private __authData: IVK.AuthData | null = null;
  private __userData: IVK.User | null = null;
  private __isLoginSuccess: boolean = false;

  public setAuthData(creditionals: IVK.AuthData) {
    this.__authData = creditionals;
  }

  private getAuthData(): IVK.AuthData | null  {
    return this.__authData;
  }

  private set setUserData(user: IVK.User) {
    this.__userData = user;
  }

  public get getUserData(): IVK.User | null {
    return this.__userData;
  }

  constructor(userCreditionals: IVK.AuthData) {
    this.setAuthData(userCreditionals);
  }

  public async doLogin(): Promise<boolean> {
    
  }
}

module.exports = VkAuth;